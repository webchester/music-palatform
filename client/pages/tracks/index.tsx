import React from 'react';
import MainLayout from '../../layout/MainLayout';
import {Grid, Card, Button, Box} from '@material-ui/core';
import { useRouter } from 'next/router';
import {TRACK_CREATE_ROUTE} from '../../utils/routes';
import {ITrack} from '../../types/track';
import TrackList from '../../components/TrackList';
import {useActions} from "../../hooks/useActions";


const Index = () => {
    const router = useRouter();
    const tracks: ITrack[] = [
        {
            "comments": [],
            "_id": "60d14146f7988d20e078bde8",
            "name": "Galkin",
            "artist": "Maksim",
            "text": "sffsflsf",
            "listens": "0",
            "picture": "http://localhost:5000/picture/50691409-3a24-4e11-bf9f-2f07cf92ca04.jpeg",
            "audio": "http://localhost:5000/audio/2cc3bfe9-b2fd-4117-b6fa-849053d162ad.mp3",

        },
        {
            "comments": [],
            "_id": "60d1433348e475246bab415f",
            "name": "Galkin",
            "artist": "Maksim",
            "text": "sffsflsf",
            "listens": "0",
            "picture": "http://localhost:5000/picture/c174b51f-98d7-49ba-9e68-3fcc06356c86.jpeg",
            "audio": "http://localhost:5000/audio/f568aafa-64ee-4032-a662-6769a0c722fa.mp3",

        },
        {
            "comments": [],
            "_id": "60d1446656880724e86e461f",
            "name": "Galkin",
            "artist": "Maksim",
            "text": "sffsflsf",
            "listens": "0",
            "picture": "http://localhost:5000/picture/3b60c828-4bc9-43b8-b5d8-af327f77bce8.jpeg",
            "audio": "http://localhost:5000/audio/52d920f1-d6ae-4750-b0ef-6253f823e958.mp3",

        },
        {
            "comments": [],
            "_id": "60d3bd874de124bb83c07abb",
            "name": "Краш",
            "artist": "Клава Кока feat. Niletto",
            "text": "no",
            "listens": "0",
            "picture": "http://localhost:5000/picture/74a30f1b-cbe9-4627-8cdc-2618491c3fc9.jpg",
            "audio": "http://localhost:5000/audio/ea2518fc-44aa-4ad7-89c3-ebbfd6173981.mp3",

        }
    ];

    return (
        <MainLayout>
            <Grid>
                <Card>
                    <Box p={3}>
                        <Grid container justifyContent="space-between">
                            <h1>Список треков</h1>
                            <Button onClick={()=>router.push(TRACK_CREATE_ROUTE)}>Загрузить</Button>
                        </Grid>
                    </Box>
                    <Box>
                        <TrackList tracks={tracks}/>
                    </Box>
                </Card>
            </Grid>
        </MainLayout>
    );
};

export default Index;