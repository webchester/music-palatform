import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId, Query } from "mongoose";
import { CreateTrackDto } from "./dto/create-track.dto";
import { Track, TrackDocument } from './schema/track.schema';
import { Comment, CommentDocument } from "./schema/comment.schema";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { FileService, FileType } from "src/file/file.service";

@Injectable()
export class TrackService{
constructor(
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
    private fileService: FileService
    ){} 

async getAll(count=10, offset=0):Promise<Track[]> {
    const foundedTracks = await this.trackModel.find().skip(Number(offset)).limit(Number(count));
    return foundedTracks;
}

async getOne(trackId: ObjectId): Promise<Track> {
    const foundedOneTrack = await this.trackModel.findById(trackId).populate('comments');
    return foundedOneTrack;
}

async create(dto: CreateTrackDto, picture, audio): Promise<Track> {
    const audioPath = this.fileService.createFile(FileType.AUDIO, audio);
    const picturePath = this.fileService.createFile(FileType.PICTURE, picture);
    const createdTrack = await this.trackModel.create({...dto, listens: 0, picture: picturePath, audio: audioPath});

    return createdTrack;
}   

async remove(trackId: ObjectId): Promise<ObjectId> {
    const removedTrack = await this.trackModel.findByIdAndDelete(trackId);
    return removedTrack._id;
}

async search(query: string): Promise<Track[]>{
    const tracksFounded = await this.trackModel.find({
        name: {$regex: new RegExp(query, 'i')}
    });
    return tracksFounded;
}

async  addComment(dto: CreateCommentDto): Promise<Comment> {
    const commentedTrack = await this.trackModel.findById(dto.trackId);

    const createdComment = await this.commentModel.create({...dto});
    await commentedTrack.comments.push(createdComment._id);
    await commentedTrack.save();
    return createdComment;
}

async listen (trackId: ObjectId){
    let listenedTrack = await this.trackModel.findById(trackId);
    listenedTrack.listens += 1;
    await listenedTrack.save();
    return listenedTrack;
}
    

}