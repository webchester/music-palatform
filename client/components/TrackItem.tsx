import React from 'react';
import { ITrack } from '../types/track';
import { Card, IconButton, Grid } from '@material-ui/core';
import { PlayArrow, Pause, Delete } from '@material-ui/icons';
import style from '../styles/TrackItem.module.scss';
import { useRouter } from 'next/router';
import {TRACK_VIEW_ROUTER} from '../utils/routes';
import {useActions} from "../hooks/useActions";

interface TrackItemProps {
    track: ITrack;
    active?: boolean;
}

const TrackItem: React.FC<TrackItemProps> = ({track, active=false}) => {
    const router = useRouter();
    const {playTrack, pauseTrack, setActiveTrack} = useActions();
    const play = (e) => {
        e.stopPropagation();
        setActiveTrack(track);
        playTrack();
    }
    return (
        <Card className={style.track} onClick={()=>router.push(TRACK_VIEW_ROUTER+track._id)}>
            <IconButton onClick={play}>
                {active
                ? <Pause/>
                : <PlayArrow/>
                }
            </IconButton>
             <img width={70} height={70} src={track.picture} alt="picture track"/>
             <Grid container direction="column" style={{width: '200px', margin: '0 20px'}}>
                 <div>{track.name}</div>
                 <div style={{fontSize: '12px', color: 'gray'}}>{track.artist}</div>
             </Grid>
             {active && <div>02:42 / 03:22</div>}
             <IconButton  onClick={e=>e.stopPropagation()}style={{marginLeft: 'auto'}}>
                 <Delete/>
             </IconButton>
        </Card>
    );
};

export default TrackItem;