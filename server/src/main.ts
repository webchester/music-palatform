import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const PORT = 5000;
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(PORT || 5000, ()=>{
    console.log(`START ON PORT ${PORT}`)
  });
}
bootstrap();
