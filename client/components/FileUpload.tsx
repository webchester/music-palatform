import React, {useRef} from 'react';

interface FileUploadProps{
    setFile: Function;
    accept: string;
}

const FileUpload:React.FC<FileUploadProps> = ({setFile, accept, children}) => {
    const ref = useRef<HTMLInputElement>();
    const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFile(e.currentTarget.files[0]);
    }
    return (
        <div onClick={ () => ref.current.click() }>
            <input
                onChange={ (e) => changeHandler(e) }
                ref={ref}
                type={'file'}
                accept={accept}
                style={{display: 'none'}}
            />
            {children}
        </div>
    );
};

export default FileUpload;