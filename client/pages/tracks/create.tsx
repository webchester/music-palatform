import React, {useState} from 'react';
import MainLayout from '../../layout/MainLayout';
import StepWrapper from "../../components/StepWrapper";
import { TextField, Button} from '@material-ui/core';
import FileUpload from "../../components/FileUpload";


const Create = () => {
    const [step, setStep] = useState(0);
    const [picture, setPicture] = useState(null);
    const [audio, setAudio] = useState(null);
    const back = () => {
        setStep(prev => prev - 1);
    };
    const next = () => {
        if(step != 2) setStep(prev => prev + 1);
    };

    return (
        <MainLayout>
           <StepWrapper activeStep={step}>
               {step === 0 &&
               <div style={{display: 'flex', flexDirection: 'column'}}>
                   <TextField  style={{marginTop: 20}} label={'Название трека'}/>
                   <TextField style={{marginTop: 20}} label={'Исполнитель'}/>
                   <TextField style={{marginTop: 20}}  multiline label={'Слова трека'}/>
               </div>
               }
               {step === 1 &&
               <FileUpload setFile={setPicture} accept={'image/*'}>
                   <Button>Загрузите обложку</Button>
               </FileUpload>
               }
               {step === 2 &&
               <FileUpload setFile={setAudio} accept={'audio/*'}>
                   <Button>Загрузите трек</Button>
               </FileUpload>
               }
           </StepWrapper>
            <div style={{display: 'flex', justifyContent:'space-between'}}>
                <Button variant={'outlined'} disabled={step==0} onClick={back}>Назад</Button>
                <Button variant={'outlined'} onClick={next}>Далее</Button>
            </div>
        </MainLayout>
    );
};

export default Create;