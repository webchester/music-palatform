import MainLayout from '../layout/MainLayout';

export default function Home() {
  return (
    <MainLayout>
    <div className="center">
      <h1>Добро пожаловать</h1>
      <h2>Здесь собраны лучшие треки</h2>
    </div>
    <style jsx>
      {`
        .center{
          margin-top: 150px;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center
        }
      `}
    </style>
    </MainLayout>
  )
}
