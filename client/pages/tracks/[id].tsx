import React from 'react';
import MainLayout from '../../layout/MainLayout';
import {ITrack} from '../../types/track';
import {Grid, Button, TextField} from '@material-ui/core';
import {useRouter} from "next/router";
import {TRACK_ROUTE} from "../../utils/routes";

const TrackView = () => {
    const router = useRouter();
    const track: ITrack = {  
    _id: '1', 
    name: 'track1', 
    artist: 'artist 1', 
    text: 'text 1', 
    listens: '0', 
    picture: 'http://localhost:5000/picture/3b60c828-4bc9-43b8-b5d8-af327f77bce8.jpeg', 
    audio: 'http://localhost:5000/audio/52d920f1-d6ae-4750-b0ef-6253f823e958.mp3',
    comments:[]
}
    return (        
        <MainLayout>
            <Button variant={"outlined"}
                    onClick={() => router.push(TRACK_ROUTE)}>
                Назад
            </Button>
            <Grid container  style={{margin: '20px 0'}}>
                <img src={track.picture} alt={'music picture'} width={200} height={200}/>
                <div style={{marginLeft: 20}}>
                    <h1>Название - {track.name} </h1>
                    <h1>Исполнитель - {track.artist} </h1>
                    <h1>Прослушиваний - {track.listens}</h1>
                </div>
            </Grid>
            <h1>Слова к треку</h1>
            <p>{track.text}</p>
            <Grid container>
                <TextField
                    label={'Ваше имя'}
                    fullWidth
                />
                <TextField
                    multiline
                    fullWidth
                    rows={4}
                    label={'Текст комментария'}
                />
                <Button>Отправить</Button>
            </Grid>
            {track.comments.map(comment =>
                <div key={comment._id}>
                    <div>Автор {comment.username}</div>
                    <div>Текст {comment.text}</div>
                </div>
            )}

        </MainLayout>
    );
};

export default TrackView;