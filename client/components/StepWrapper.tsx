import React from 'react';
import {Grid, Step, Container, StepLabel, Card, Stepper} from '@material-ui/core';

interface StepWrapperProps{
    activeStep: number;
}
const steps=[
    'Информация о треке', "Загрузка обложки", "Загрузка трека"
]
const StepWrapper:React.FC<StepWrapperProps> = ({activeStep, children}) => {
    return (
        <Container>
            <Stepper activeStep={activeStep}>
                {steps.map( (step, index) => <Step key={index} completed={activeStep > index}>
                    <StepLabel>
                        {step}
                    </StepLabel>
                </Step>)}
            </Stepper>
            <Grid container  justifyContent={'center'} style={{margin: '30px',  height: '270px'}}>
                <Card style={{padding: 20, width: '600px'}}>
                    <h2>{steps[activeStep]}</h2>
                    {children}
                </Card>
            </Grid>
        </Container>
    );
};

export default StepWrapper;