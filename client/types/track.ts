import { Icomment } from "./comment";

export interface ITrack{
    _id: string;
    name: string;
    artist: string;
    text: string;
    listens: string;
    picture: string;
    audio: string;
    comments: Icomment[];
}