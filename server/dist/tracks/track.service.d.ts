import { Model, ObjectId } from "mongoose";
import { CreateTrackDto } from "./dto/create-track.dto";
import { Track, TrackDocument } from './schema/track.schema';
import { Comment, CommentDocument } from "./schema/comment.schema";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { FileService } from "src/file/file.service";
export declare class TrackService {
    private trackModel;
    private commentModel;
    private fileService;
    constructor(trackModel: Model<TrackDocument>, commentModel: Model<CommentDocument>, fileService: FileService);
    getAll(count?: number, offset?: number): Promise<Track[]>;
    getOne(trackId: ObjectId): Promise<Track>;
    create(dto: CreateTrackDto, picture: any, audio: any): Promise<Track>;
    remove(trackId: ObjectId): Promise<ObjectId>;
    search(query: string): Promise<Track[]>;
    addComment(dto: CreateCommentDto): Promise<Comment>;
    listen(trackId: ObjectId): Promise<TrackDocument>;
}
