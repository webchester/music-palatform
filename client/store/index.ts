// create a makeStore function
import {createStore, Store} from "redux";
import {Context, createWrapper} from "next-redux-wrapper";
import {reducer, RootState} from "./reducers";
// create a makeStore function
const makeStore = (context: Context) => createStore(reducer);

// export an assembled wrapper
export const wrapper = createWrapper<Store<RootState>>(makeStore, {debug: true});