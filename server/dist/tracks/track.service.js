"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const track_schema_1 = require("./schema/track.schema");
const comment_schema_1 = require("./schema/comment.schema");
const file_service_1 = require("../file/file.service");
let TrackService = class TrackService {
    constructor(trackModel, commentModel, fileService) {
        this.trackModel = trackModel;
        this.commentModel = commentModel;
        this.fileService = fileService;
    }
    async getAll(count = 10, offset = 0) {
        const foundedTracks = await this.trackModel.find().skip(Number(offset)).limit(Number(count));
        return foundedTracks;
    }
    async getOne(trackId) {
        const foundedOneTrack = await this.trackModel.findById(trackId).populate('comments');
        return foundedOneTrack;
    }
    async create(dto, picture, audio) {
        const audioPath = this.fileService.createFile(file_service_1.FileType.AUDIO, audio);
        const picturePath = this.fileService.createFile(file_service_1.FileType.PICTURE, picture);
        const createdTrack = await this.trackModel.create(Object.assign(Object.assign({}, dto), { listens: 0, picture: picturePath, audio: audioPath }));
        return createdTrack;
    }
    async remove(trackId) {
        const removedTrack = await this.trackModel.findByIdAndDelete(trackId);
        return removedTrack._id;
    }
    async search(query) {
        const tracksFounded = await this.trackModel.find({
            name: { $regex: new RegExp(query, 'i') }
        });
        return tracksFounded;
    }
    async addComment(dto) {
        const commentedTrack = await this.trackModel.findById(dto.trackId);
        const createdComment = await this.commentModel.create(Object.assign({}, dto));
        await commentedTrack.comments.push(createdComment._id);
        await commentedTrack.save();
        return createdComment;
    }
    async listen(trackId) {
        let listenedTrack = await this.trackModel.findById(trackId);
        listenedTrack.listens += 1;
        await listenedTrack.save();
        return listenedTrack;
    }
};
TrackService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel(track_schema_1.Track.name)),
    __param(1, mongoose_1.InjectModel(comment_schema_1.Comment.name)),
    __metadata("design:paramtypes", [mongoose_2.Model,
        mongoose_2.Model,
        file_service_1.FileService])
], TrackService);
exports.TrackService = TrackService;
//# sourceMappingURL=track.service.js.map