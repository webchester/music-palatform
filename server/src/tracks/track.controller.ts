import { UploadedFiles, UseInterceptors } from "@nestjs/common";
import { Controller, Post, Query, Delete, Get, Body, Param } from "@nestjs/common";
import { FileFieldsInterceptor } from "@nestjs/platform-express";
import { ObjectId } from "mongoose";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { CreateTrackDto } from "./dto/create-track.dto";
import { TrackService } from "./track.service";


@Controller('track')
export class TrackController {
    constructor(private trackService: TrackService){}

    @Post()
    @UseInterceptors(FileFieldsInterceptor([
        {name: 'picture', maxCount:1},
        {name: 'audio', maxCount:1}
    ]))
    create(@UploadedFiles() files, @Body() dto: CreateTrackDto){
        const {picture, audio} = files;
        return  this.trackService.create(dto, picture[0], audio[0]);
    }

    @Get()
    getAll( 
        @Query('count') count: number,
        @Query('limit') offset: number):Promise<CreateTrackDto[]>{
        return this.trackService.getAll(count, offset);
    }

    @Get('search')
    search(@Query('query') query: string){
        return this.trackService.search(query);
    }

    @Get(':trackId')
    getOne(@Param('trackId') trackId: ObjectId ){
        return  this.trackService.getOne(trackId);
    }

    @Delete(':trackId')
    remove(
        @Param('trackId') trackId: ObjectId
    ){
        return this.trackService.remove(trackId);
    }

    @Post("/comment")
    addComment(@Body() dto: CreateCommentDto){
        return this.trackService.addComment(dto);
    }

    @Post("/listens/:trackId")
    listen(@Param('trackId') trackId: ObjectId){
        return this.trackService.listen(trackId);
    }
}