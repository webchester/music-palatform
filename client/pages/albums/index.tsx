import React from 'react';
import MainLayout from '../../layout/MainLayout';

const Index = () => {
    return (
        <MainLayout>
            <h1>ALBUMS</h1>
        </MainLayout>
    );
};

export default Index;