
    //TRACK
export const TRACK_ROUTE = '/tracks';
export const TRACK_CREATE_ROUTE = '/tracks/create';
export const TRACK_VIEW_ROUTER = '/tracks/';

    //ALBUM
export const ALBUMS_ROUTE = '/albums';

    //OTHER ROUTES
export const HOME_ROUTE = '/';
