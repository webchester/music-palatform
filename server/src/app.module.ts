import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FileModule } from './file/file.module';
import { TrackModule } from './tracks/track.module';
import  { ServeStaticModule } from '@nestjs/serve-static';
import * as path from 'path';
@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://admin:admin@cluster0.q1zzh.mongodb.net/music?retryWrites=true&w=majority'),
    TrackModule, 
    FileModule,
    ServeStaticModule.forRoot({rootPath: path.resolve(__dirname, 'static') })
  ]
})
export class AppModule {}
